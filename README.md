# pacGet

A fully foss, lightweight fuzzysearching TUI frontend for pacstall using fzf

fzf https://github.com/junegunn/fzf  MIT License

pacstall https://github.com/pacstall/pacstall  GPL-3.0 License

name is subject to change
