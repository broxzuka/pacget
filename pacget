#!/usr/bin/env bash
#-*- mode: sh ;mode:lsp;-*-

#begin settings
STGDIR="${HOME}/.local/share/pacstall"

listfile="${HOME}/.local/share/pacget/listfile"

export BOLD=$(tput bold)
export NORMAL=$(tput sgr0)
export NC='\033[0m'
export RED='\033[0;31m'          # Red
export GREEN='\033[0;32m'        # Green
export BIRed='\033[1;91m'        # Red
export BIGreen='\033[1;92m'      # Green
export BIYellow='\033[1;93m'     # Yellow
function ask() {
  local prompt default reply
  if [[ ${2:-} = 'Y' ]]; then
    prompt="${BIGreen}Y${NC}/${RED}n${NC}"
    default='Y'
  elif [[ ${2:-} = 'N' ]]; then
    prompt="${GREEN}y${NC}/${BIRed}N${NC}"
    default='N'
  else
    prompt="${GREEN}y${NC}/${RED}n${NC}"
  fi
  while true; do
    echo -ne "$1 [$prompt] "
    if [[ -z "$DISABLE_PROMPTS" ]]; then
      read -r reply < /dev/tty
    else
      echo "$default"
      reply=$default
    fi
    if [[ -z $reply ]]; then
      reply=$default
    fi
    case "$reply" in
      Y*|y*) return 0 ;;
      N*|n*) return 1 ;;
    esac
  done
}
export -f ask
function fancy_message() {
  local MESSAGE_TYPE="${1}"
  local MESSAGE="${2}"
  case ${MESSAGE_TYPE} in
    info) echo -e "[${BGreen}+${NC}] INFO: ${MESSAGE}";;
    warn) echo -e "[${BYellow}*${NC}] WARNING: ${MESSAGE}";;
    error) echo -e "[${BRed}!${NC}] ERROR: ${MESSAGE}";;
    *) echo -e "[${BOLD}?${NORMAL}] UNKNOWN: ${MESSAGE}";;
  esac
}
export -f fancy_message
function loadpackagelists(){
  PACKAGELIST=()
  URLLIST=()
  LIST=()
  while IFS= read -r URL; do
    PARTIALLIST=($(curl -s "$URL"/packagelist))
    URLLIST+=("${PARTIALLIST[@]/*/$URL}")
    PACKAGELIST+=(${PARTIALLIST[@]})
  done < "$STGDIR/repo/pacstallrepo.txt"
  for i in ${!PACKAGELIST[@]}; do
    SPLIT=($(echo "${URLLIST[$i]}" | tr "/" "\n"))
    if command echo "${URLLIST[$i]}" |grep "github" &> /dev/null; then
      REPO="${SPLIT[-3]}"    
    elif command echo "${URLLIST[$i]}" |grep "gitlab" &> /dev/null; then
      REPO="${SPLIT[-4]}"
    else
      REPO="$REPO"
    fi
    LIST+=("${PACKAGELIST[$i]}@$REPO\t\t\t\t\t\t\t\t\t\t\t\t\t|${PACKAGELIST[$i]}|${URLLIST[$i]}")
  done
  echo -e "${LIST[@]}"|tr -s ' ' '\n'|tr '|' ' '| sort -u > $listfile
}
#end settings

if [[ ! -f $listfile ]]; then
  mkdir -p "${HOME}/.local/share/pacget/"
  loadpackagelists
fi

case $1 in
    "-r"|"--refresh")
    loadpackagelists
  ;;
  "-h"|"--help")
    echo "
    options:
    -r, --refresh,  update packagelist"
    exit 0
  ;;
  
esac


selected=$(cat $listfile |fzf --ansi --cycle --preview="echo -e {1} && echo "" && curl -s {3}/packages/{2}/{2}.pacscript" --preview-window=right:60%:wrap)
#|pygmentize -l bash#add this to the line above after{}.pacscript to get syntax highlighting
echo $selected

if [ -z ${selected} ]; then
  exit 0
else
  opts="Yes
no"
  
  IFS=' '
  INFO=($selected)
  option=$(printf "%s\n%s" $opts| fzf --cycle --header="install ${INFO[0]} ?")
  
  if [[ $option = "Yes" ]]; then
    echo -e  "Installing $BOLD${INFO[0]}$NC"
    
    PACKAGE=${INFO[1]} 
    REPO=${INFO[2]}
  
    sudo pacget_install $PACKAGE $REPO
    
  elif [[ $option = "no" ]]; then
    echo "exiting";
    exit 0
  fi
fi
